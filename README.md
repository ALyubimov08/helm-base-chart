# Helm Charts

##### Variables in commands below:
* `USERNAME` - username from gitlab with user who have access to pull this package
* `TOKEN` - user's token with `read-repository` permission

----

##### Add repository to local machine:
```shell
helm repo add --username USERNAME --password TOKEN axarea https://gitlab.axarea.ru/api/v4/projects/269/packages/helm/stable
```

##### Load the contents of repositories:
```shell
helm repo update
```

##### List all charts and their versions:
```shell
helm search repo -l axarea
```


##### Install application with custom `values.yaml`:
```shell
helm install app-name axarea/base -f values.yaml
```


##### Local templating for debug:
```shell
helm template charts/base > base.yaml
```


### More information:
```shell
helm repo [command]

Available Commands:
  add         add a chart repository
  index       generate an index file given a directory containing packaged charts
  list        list chart repositories
  remove      remove one or more chart repositories
  update      update information of available charts locally from chart repositories

Flags:
  -h, --help   help for repo

Global Flags:
      --burst-limit int                 client-side default throttling limit (default 100)
      --debug                           enable verbose output
      --kube-apiserver string           the address and the port for the Kubernetes API server
      --kube-as-group stringArray       group to impersonate for the operation, this flag can be repeated to specify multiple groups.
      --kube-as-user string             username to impersonate for the operation
      --kube-ca-file string             the certificate authority file for the Kubernetes API server connection
      --kube-context string             name of the kubeconfig context to use
      --kube-insecure-skip-tls-verify   if true, the Kubernetes API server's certificate will not be checked for validity. This will make your HTTPS connections insecure
      --kube-tls-server-name string     server name to use for Kubernetes API server certificate validation. If it is not provided, the hostname used to contact the server is used
      --kube-token string               bearer token used for authentication
      --kubeconfig string               path to the kubeconfig file
  -n, --namespace string                namespace scope for this request
      --registry-config string          path to the registry config file (default "/Users/masterbpro/Library/Preferences/helm/registry/config.json")
      --repository-cache string         path to the file containing cached repository indexes (default "/Users/masterbpro/Library/Caches/helm/repository")
      --repository-config string        path to the file containing repository names and URLs (default "/Users/masterbpro/Library/Preferences/helm/repositories.yaml")

```